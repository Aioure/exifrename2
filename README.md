# EXIF ReName

# A program to name the pictures after the date and time the picture was taken.

Uses Qt5. For Linux, Windows, macOS, Android or embedded systems.

<h2>[Read more](https://gitlab.com/posktomten/exifrename2/-/wikis/Home)</h2>

<table><tr><td>

Dependencies

<a href="https://gitlab.com/posktomten/libcheckforupdates">libcheckforupdates</a>

<a href="https://gitlab.com/posktomten/libzsyncupdateappimage">libzsyncupdateappimage</a>

</td></tr></table>
