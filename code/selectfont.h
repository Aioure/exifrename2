//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          EXIF ReName
//          Copyright (C) 2007 - 2021 Ingemar Ceicer
//          https://gitlab.com/posktomten/exifrename2
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#ifndef SELECTFONT_H
#define SELECTFONT_H

#include "info.h"
#include <QDebug>
#include <QDialog>
#include <QSettings>
#define THIS_WIDHT 671
#define THIS_HEIGHT 140
#define DEFAULT_FAMILY "Ubuntu"

QT_BEGIN_NAMESPACE
namespace Ui
{
class SelectFont;
}
QT_END_NAMESPACE

class SelectFont : public QDialog
{
    Q_OBJECT

private:
    Ui::SelectFont *ui;
    void triggerSignal();

public:
    SelectFont(QWidget *parent = nullptr);
    ~SelectFont();

signals:
    void valueChanged(QFont font);
    void selectFontClose();
};
#endif // SELECTFONT_H
