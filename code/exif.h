//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          EXIF ReName
//          Copyright (C) 2007 - 2021 Ingemar Ceicer
//          https://gitlab.com/posktomten/exifrename2
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#ifndef EXIFH
#define EXIFH
//#include "mainwindow.h"
#include <QCoreApplication>
#include <QFileInfo>
#include <QDebug>
#include <string>
using namespace std;

class Exif
{
    Q_DECLARE_TR_FUNCTIONS(Exif)
private:

    void format_good(char *s);
    void finn(char *hela, char *hittat);
    bool s(char c);
    bool sa(char c);
    bool sb(char c);

    char sep;
    char dt_sep;

public:
    Exif();
    bool getExif(QString &filnamn, char *hittat, QString *info);
};
#endif // EXIFH
