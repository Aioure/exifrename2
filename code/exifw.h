//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          EXIF ReName
//          Copyright (C) 2007 - 2021 Ingemar Ceicer
//          https://gitlab.com/posktomten/exifrename2
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#ifndef EXIFW_H
#define EXIFW_H

#include "mainwindow.h"
#include "info.h"
using namespace std;

class Exifw
{

    Q_DECLARE_TR_FUNCTIONS(Exifw)

private:

    bool finn(char *hela, int ganger);
    bool s(char c);
    bool sa(char c);
    bool sb(char c);
    int raknare;
    bool manipulera(char *filnamn, char *bytut, char *info);
    char sep;
    char dt_sep;
    char *funnit;

public:
    Exifw();
    bool setExif(char *filnamn, char *hittat, char *info);




};

#endif // EXIFW_H
